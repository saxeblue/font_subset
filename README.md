# WEB FONT のサブセット

参考サイト  
https://ureta.net/2017/02/tool_fonttools/

## fonttools ソースコードをダウンロード
```
git clone https://github.com/fonttools/fonttools.git
```

## fonttools ディレクトリに移動
```
cd fonttools
```

## 仮想環境を作る
```
python -m virtualenv fonttools-venv
```

## 仮想環境を有効化
```
. fonttools-venv/bin/activate
```

## -e オプションをつけてローカルでインストール
```
pip install -e .
```

## brotli をインストール
```
pip install --upgrade git+https://github.com/google/brotli
```

## フォントのサブセット  
woffとwoff2で、--flavorを書き換える

- woff
```
pyftsubset ./_src/NotoSansCJKjp-Black.otf --text-file=./_src/chars.txt --layout-features='*' --flavor=woff --output-file=./_dist/NotoSansCJKjp-Black.woff
```

- woff2
```
pyftsubset ./_src/NotoSansCJKjp-Black.otf --text-file=./_src/chars.txt --layout-features='*' --flavor=woff2 --output-file=./_dist/NotoSansCJKjp-Black.woff2
```

## CSS設定
```
@font-face {
	font-family: 'example';
	src: url('example.woff2') format('woff2'), 
		 url('example.woff') format('woff');
}
```
